import { StyleSheet, Text, View } from "react-native";
import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-native-elements";

const Wrapper = styled.View``;
const Container = styled.View``;
const Month = styled.View``;
const Header = styled.View``;
const Content = styled.View`
  color: white;
`;
const Logo = styled.View``;

class Card extends Component {
  state = {};
  render() {
    return (
      <>
        <Wrapper style={styles.Container}>
          <Logo style={styles.logo}></Logo>

          <Container style={styles.Wrapper}>
            <Month style={styles.Month}>
              <Text style={{ color: "#fff", fontSize: 20 }}>SEPTEMBER 1 3</Text>
            </Month>

            <Header style={styles.Header}>
              <Text style={{ color: "#c2bebc", fontSize: 12 }}>
                DAILY HOROSCOPE
              </Text>
            </Header>
            <Content style={styles.Content}>
              <Text style={{ color: "white", fontSize: 16 }}>
                A problem could come out this morning. Be careful of the words
                you choose. It appears as if you will be playing kiss and make
                up for the remainer of the day. You see progress and a new
                alignment occuring. Tonight: Finally smiling.
              </Text>
            </Content>
          </Container>
        </Wrapper>
      </>
    );
  }
}

const styles = {
  Wrapper: {
    width: "90%",
    height: "100%",
    marginTop: 30,
    backgroundColor: "#9396803b",
    borderRadius: 10
  },

  logo: {
    position: "absolute",
    width: 45,
    height: 45,
    // top: 60,
    marginTop: 5,

    zIndex: -2,
    alignItems: "center",
    backgroundColor: "#d69b5b3b",
    borderRadius: 50
  },
  Container: {
    zIndex: -1,
    width: "75%",
    height: 220,
    marginTop: 100,
    borderRadius: 10,
    marginLeft: 105,
    opacity: 10,
    alignItems: "center",
    position: "relative"
  },
  Month: {
    marginTop: 20,
    alignItems: "center"
  },
  Header: {
    alignItems: "center",
    fontSize: 800
  },
  Content: {
    padding: 15,
    color: "white"
  }
};
export default Card;
