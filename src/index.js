import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Ball from "./Ball";
import Card from "./card";
import ShadowCard from "./ShadowCard";
import ShadowCard2 from "./ShadowCard2";
import styled from "styled-components";
import { Icon } from "react-native-elements";

const Box = styled.View``;
const HeaderContainer = styled.View``;
export default function App() {
  return (
    <View style={styles.container}>
      <HeaderContainer style={styles.headerContainer}>
        <Icon
          name="settings"
          color="grey"
          style={{ marginLeft: 60, fontSize: 60 }}
          onPress={() => alert("hello")}
        />

        <Text
          style={{
            color: "#80bfff",
            fontSize: 16,
            marginLeft: "25%",
            justifyContent: "space-between"
          }}
        >
          Frankie Marshals
        </Text>
      </HeaderContainer>
      <HeaderContainer style={styles.natalContainer}>
        <Text
          style={{
            color: "grey",
            fontSize: 15
          }}
        >
          VIEW NATAL CHART
        </Text>
      </HeaderContainer>

      <Ball />
      <Box style={styles.box}>
        <Box style={styles.innerbox}>
          <ShadowCard2 />
        </Box>

        <ShadowCard />
      </Box>
      <Card />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 50
    // justifyContent: "center"
  },
  box: {
    marginTop: 200,
    position: "absolute"
  },
  innerbox: {
    marginTop: 0,
    position: "absolute"
  },
  headerContainer: {
    width: "100%",
    display: "flex",
    paddingLeft: 20,
    marginRight: 20,
    // justifyContent: "center",
    flexDirection: "row"
  },
  natalContainer: {
    width: "50%",
    borderWidth: 0.5,
    borderColor: "grey",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    marginTop: 8,
    padding: 5,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 20,
    paddingRight: 20
  }
});
