import { StyleSheet, Text, View } from "react-native";
import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-native-elements";

const Wrapper = styled.View``;
const Container = styled.View``;
const Month = styled.View``;
const Header = styled.View``;
const Content = styled.View`
  color: white;
`;
const Logo = styled.View``;

class Card extends Component {
  state = {};
  render() {
    return (
      <>
        <Wrapper style={styles.Container}></Wrapper>
      </>
    );
  }
}

const styles = {
  Container: {
    zIndex: -2,
    width: "75%",
    height: 150,
    backgroundColor: "#9396803b",
    marginTop: 100,
    borderRadius: 10,
    marginLeft: 210,
    opacity: 10,
    alignItems: "center",
    position: "relative"
  }
};
export default Card;
