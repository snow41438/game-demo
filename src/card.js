import { StyleSheet, Text, View } from "react-native";
import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-native-elements";
import Svg, { Image } from "react-native-svg";
const Wrapper = styled.View``;
const Container = styled.View``;
const Month = styled.View``;
const Header = styled.View``;
const Content = styled.View`
  color: white;
`;
const Logo = styled.View``;
const IconBar = styled.View``;

class Card extends Component {
  state = {};
  render() {
    return (
      <>
        <Wrapper style={styles.Container}>
          <Logo style={styles.logo}></Logo>

          <Container style={styles.Wrapper}>
            <View style={styles.IconBar}>
              <Icon name="ios-calendar" color="#fff" />
              <Icon name="share" color="#fff" />
            </View>

            <Month style={styles.Month}>
              <Text style={{ color: "white", fontSize: 20 }}>
                SEPTEMBER 1 3
              </Text>
            </Month>
            <Header style={styles.Header}>
              <Text style={{ color: "#c2bebc", fontSize: 12 }}>
                DAILY HOROSCOPE
              </Text>
            </Header>
            <Content style={styles.Content}>
              <Text style={{ color: "white", fontSize: 16 }}>
                A problem could come out this morning. Be careful of the words
                you choose. It appears as if you will be playing kiss and make
                up for the remainer of the day. You see progress and a new
                alignment occuring. Tonight: Finally smiling.
              </Text>
            </Content>
          </Container>
        </Wrapper>
      </>
    );
  }
}

const styles = {
  Wrapper: {
    width: "100%",
    height: "100%",
    marginTop: 30,
    backgroundColor: "grey",
    borderRadius: 10
  },
  IconBar: {
    width: "100%",
    height: 25,
    justifyContent: "space-between",
    padding: 20,
    flexDirection: "row",
    display: "flex"
  },
  logo: {
    position: "absolute",
    width: 60,
    height: 60,
    // top: 60,
    zIndex: 2,
    alignItems: "center",
    backgroundColor: "#ff8c66",
    borderRadius: 50
  },
  Container: {
    width: "80%",
    height: 240,
    marginTop: 40,
    borderRadius: 10,
    alignItems: "center",
    position: "relative"
  },
  Month: {
    // marginTop: 5,
    alignItems: "center"
  },
  Header: {
    alignItems: "center",
    fontSize: 800
  },
  Content: {
    paddingTop: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 60,
    color: "white"
  }
};
export default Card;
