import React, { Component } from "react";
import styled from "styled-components";

import { Animated, PanResponder } from "react-native";
import Svg, {
  Circle,
  Image,
  Defs,
  ClipPath,
  TextPath,
  TSpan,
  G,
  Path
} from "react-native-svg";

const Wrapper = styled.View``;
const Container = styled.View``;
const Button = styled.View``;

class Ball extends Component {
  // state = { panResponder };

  constructor(props) {
    super(props);

    panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (event, gesture) => {
        console.log("gesture", gesture);
      },
      onPanResponderMove: (event, gestureState) => {
        // console.log("gesture", gesture);
      },
      onPanResponderRelease: (evt, gestureState) => {
        Animated.spring(this.state.scale, {
          toValue: { x: 1, friction: 3 }
        }).start();
      },
      onPanResponderGrant: (evt, gestureState) => {
        Animated.spring(this.state.scale, {
          toValue: { x: 1.3, friction: 3 }
        }).start();
      }
    });

    this.state = {
      panResponder,
      scale: new Animated.Value(1),
      pan: new Animated.ValueXY()
    };
  }
  componentWillMount() {
    this.position = new Animated.ValueXY(0, 0);
    Animated.spring(this.position, {
      toValue: { x: 200, y: 500 }
    }).start();
  }
  render() {
    console.log("state ", this.state.panResponder);
    return (
      <Wrapper
        style={styles.first}
        {...this.state.panResponder.panHandlers}
        onclick={console.log("touched")}
      >
        <Animated.View style={this.position.getTranslateTransform}>
          <Button style={styles.mbutton}></Button>
        </Animated.View>

        {/* <Container style={styles.container}> */}
        <Wrapper style={styles.second}>
          <Svg height="100%" width="100%" viewBox="0 0 100 100">
            <Defs>
              <ClipPath id="clip">
                <Circle cx="50" cy="40" r="50" />
              </ClipPath>
            </Defs>

            <Image
              width="100%"
              height="100%"
              // opacity="0.5"
              href={require("../assets/img.jpeg")}
              clipPath="url(#clip)"
              style={styles.imageStyle}
            />
          </Svg>
        </Wrapper>
        {/* </Container> */}
      </Wrapper>
    );
  }
}

const styles = {
  first: {
    height: 200,
    width: 200,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "#DCDBDB",
    marginTop: "8%"
  },
  imageStyle: {
    // width: 100,
    // height: 100
    borderRadius: 90,
    // padding: 10
    margin: 1
  },
  innerball: {
    height: 100,
    width: 100,
    borderRadius: 50,
    borderWidth: 10,
    borderColor: "#000",
    color: "red",
    marginLeft: 20
  },
  second: {
    height: 130,
    width: 130,
    borderRadius: 90,
    borderWidth: 13,
    // zIndex: 2,
    borderColor: "#b5532270",

    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "10%",
    marginBottom: "50%"
    // backgroundColor: "red"
    // padding: 1
  },
  container: {
    width: "100%",
    height: "100%"
  },
  mbutton: {
    width: 25,
    height: 25,
    borderRadius: 15,
    backgroundColor: "#00ace6",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "-5%",
    zIndex: 10
  }
};

export default Ball;
