import React from "react";
import { StyleSheet, Text, View } from "react-native";

import Display from "./src/index";

export default function App() {
  return <Display />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 50
  }
});
